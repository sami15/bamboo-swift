//
//  Tangler.swift
//  bamboo
//
//  Created by toppantaro on 2019/08/28.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation

class Tangler {
    
    //come up with a tangle array for liked or replied post
    func createTangle ( credentials: YamfSignatory, segment : BambooEntry , everything : [BambooEntry] ) -> BambooContainerTangle? {
        
        //have a previous steps that filters everything to ID's who have had interactions with posts
        
        var tpairs = [TanglePair]()
        //filter out own stuff
        let notMine = everything.filter { $0.author != credentials }
        if notMine.isEmpty { return nil }
        
        //reverse array to see from latest to oldest
        
        //get latest hash - seqnum pairs of VERIFIED logs
        
        //lipmaa linked entries preferred
        
        //friendships should affect this
        
        //no guarantee of sorted seqnums as of now
        let notMineReversed = notMine.reversed()
        var previousAuthor = segment.author
        for i in notMineReversed {
            //just pick the latest one
            if i.author != previousAuthor {
                
                tpairs.append(TanglePair(author: i.author.publicKey,sequenceNumber: i.sequenceNumber))
                
                previousAuthor = i.author
            }
        }
        print (tpairs)
        //check for size
        return BambooContainerTangle(data: tpairs)
        
    }
    
    func unTangle (data : [BambooPayload] ){
        //use topo sort newest to olders
        let hasTangleData =  data.filter { $0.container.tangle != nil }
        let tite = hasTangleData.map{ $0.container.data}
        print(tite)
        
    }
    
    
}
