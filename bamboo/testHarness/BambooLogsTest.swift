//
//  BambooLogsTest.swift
//  bamboo
//
//  Created by toppantaro on 2019/08/20.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation
import Interstellar

class BambooLogsTest {
    
    let logs = BambooLogs()
    let payloadStore = PayloadStore()
    let tangler = Tangler()
    //let dataFromUDP = Observable<[UInt8]>()
    //let dataToUDP = Observable<[UInt8]>()
    //let worldSignal = Observable<BambooWorldSignals>()  //ui etc
    
    var io : BambooUDPSocket? = nil
    
    func test() {
        
        io = BambooUDPSocket(dataIn : dataFromUDP , dataOut : dataToUDP )
        io?.initialize()
        
        worldSignal.subscribe { signal in
            self.eventSignal(signal: signal)
        }
        /* for  f in 1...41 {
            var ff = UInt64(f)
            let mu = logs.calcLipmaa(seq: ff)
            print("\(ff) \(mu) ")
            
        } */
        
        let userSignatory = YamfSignatory(publicKeyInHex: "BADF00D")
        
        for i in 1...41 {
            let title = "BADF00DTest" + String(i)
            let payload = payloadStore.payloadHelper(containerType: .post, target: nil, tangle: nil, text: title )
            appendEntry(sig: userSignatory, payload: payload )
            payloadStore.append(payload: payload)
            
        }
        
        let userSignatory2 = YamfSignatory(publicKeyInHex: "DEADABBA")
        
        for i in 1...14 {
            let title = "DEADABBA" + String(i)
            let payload = payloadStore.payloadHelper(containerType: .post, target: nil, tangle: nil, text: title )
            appendEntry(sig: userSignatory2, payload: payload )
            payloadStore.append(payload: payload)
            
        }
        
        let userSignatory3 = YamfSignatory(publicKeyInHex: "F00DF00F")
        for i in 1...4 {
            let title = "F00DF00F" + String(i)
            let payload = payloadStore.payloadHelper(containerType: .post, target: nil, tangle: nil, text: title )
            appendEntry(sig: userSignatory3, payload: payload )
            payloadStore.append(payload: payload)
            
        }
        
        
        if let likedEntry = logs.findSegmentByPubkeyAndSeqnum( credentials : userSignatory , seq : 4 ) {
            let likedTanglePair = TanglePair(author: likedEntry.author.publicKey, sequenceNumber: 4)
            //like Deadabbas post
            let tanglelike = tangler.createTangle(credentials : userSignatory ,segment: likedEntry, everything: logs.everything )
            let likedPayload = payloadStore.payloadHelper(containerType: .like, target: likedTanglePair, tangle: tanglelike, text: "DEADABBA loves seq 4" )
            appendEntry(sig: userSignatory2, payload: likedPayload )
            payloadStore.append(payload: likedPayload)
            
            //dont force tangle on every little thing
            //maybe just on lipmaa slots
            
            //BADFOOD comments on the like
            let tanglecomm = tangler.createTangle(credentials : userSignatory ,segment: likedEntry, everything: logs.everything )
            let commentPayload = payloadStore.payloadHelper(containerType: .comment, target: likedTanglePair, tangle: tanglecomm, text: "thanks for liking seq 4 DEADABBA" )
            appendEntry(sig: userSignatory, payload: commentPayload )
            payloadStore.append(payload: likedPayload)
            
            
        }
        
        //more shitposting
        
        for i in 1...11 {
            let title = "BADF00DTestMORE" + String(i)
            let payload = payloadStore.payloadHelper(containerType: .post, target: nil, tangle: nil, text: title )
            appendEntry(sig: userSignatory, payload: payload )
            payloadStore.append(payload: payload)
            
        }
        
        for i in 1...7 {
            let title = "F00DF00FtestMORE" + String(i)
            let payload = payloadStore.payloadHelper(containerType: .post, target: nil, tangle: nil, text: title )
            appendEntry(sig: userSignatory3, payload: payload )
            payloadStore.append(payload: payload)
            
        }
        
        if let likedEntry2 = logs.findSegmentByPubkeyAndSeqnum( credentials : userSignatory , seq : nil ) {
            //passing nil returns the latest seqnum
            let tanglelike2 = tangler.createTangle(credentials : userSignatory3 ,segment: likedEntry2, everything: logs.everything )
            let likedTanglePair2 = TanglePair(author: likedEntry2.author.publicKey, sequenceNumber: likedEntry2.sequenceNumber)
            
            let likedPayload = payloadStore.payloadHelper(containerType: .like, target: likedTanglePair2, tangle: tanglelike2, text: "BADF00D loves latest seq" )
            appendEntry(sig: userSignatory2, payload: likedPayload )
            payloadStore.append(payload: likedPayload)
        }
        
        
        let tanglecomm = tangler.unTangle(data: payloadStore.everything )
        
        
        //appendEntry(sig: userSignatory, payload: "dustytest2")
        //appendEntry(sig: userSignatory, payload: "dustytest3")
        /*
        let userSignatory2 = YamfSignatory(publicKeyInHex: "ABBAABBA")
        appendEntry(sig: userSignatory, payload: "hutchtest1")
        appendEntry(sig: userSignatory, payload: "hutchtest2")
        appendEntry(sig: userSignatory, payload: "hutchtest3") */
        
    }
    
    
    
    func appendEntry ( sig : YamfSignatory , payload : BambooPayload ) {
        
        let seq = logs.findAvailableSeqNumber(credentials: sig )
        var ent = logs.createNewEntryHelper(credentials: sig, payload: payload, seq: seq)
        ent.sign(privateKey: "test")
        _ = logs.append(entry: ent)
        
    }
    
    func eventSignal (signal : BambooWorldSignals ) {
        
        switch signal {
        case .sendAButton:
            sendAbutton()
        case .sendBButton:
            sendBbutton()
        case .updateSegmentList:
            doNothing()
        default:
            doNothing()
        }
        
    }
    
    func doNothing(){
        
    }
    func sendAbutton(){
        //let dataFromUDP = Observable<[UInt8]>()
        //let dataToUDP = Observable<[UInt8]>()
        
        let allEntries = logs.getAllEntries(userNumber: 0)
        for i in allEntries {
            let bambooFrame = BambooFrame(segment:i)
            dataToUDP.update(bambooFrame.data)
            break
        }
    }
    func sendBbutton(){
        let allEntries = logs.getAllEntries(userNumber: 1)
        for i in allEntries {
            let bambooFrame = BambooFrame(segment:i)
            dataToUDP.update(bambooFrame.data)
            break
        }
    }
    
    
}
