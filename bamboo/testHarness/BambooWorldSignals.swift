//
//  BambooWorldSignals.swift
//  bamboo
//
//  Created by toppantaro on 2019/08/28.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation

enum BambooWorldSignals {
    case sendAButton   //sends test data A
    case sendBButton
    case updateSegmentList //new segments received
}
