//
//  SwiftSocket.swift
//  bamboo
//
//  Created by toppantaro on 2019/08/28.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation
import Network
import UDPBroadcast
import Interstellar
class BambooUDPSocket {
    
    //https://github.com/gunterhager/UDPBroadcastConnection


    let dataFromUDP : Observable<[UInt8]>
    let dataToUDP : Observable<[UInt8]>
    var conn : UDPBroadcastConnection? = nil
    
    init( dataIn : Observable<[UInt8]> , dataOut : Observable<[UInt8]> ){
        
        dataFromUDP = dataIn
        dataToUDP = dataOut
        
        
        
        
        
        
        dataOut.subscribe { datty in
            //let d = Data(bytes: datty, count: datty.count )
            //self.sendUDP(d)
            self.sendUDP(bytes : datty)
            
        }

        
    }
    
    func initialize () {
        
        do {
            conn = try UDPBroadcastConnection(
                port: 8680,
                handler: { [weak self] (ipAddress: String, port: Int, response: Data) -> Void in
                    guard let self = self else { return }
                    
                    
                    let hexString = self.hexBytes(data: response)
                    let utf8String = String(data: response, encoding: .utf8) ?? ""
                    print("UDP connection received from \(ipAddress):\(port):\n\(hexString)\n\(utf8String)\n")
                    let binz = [UInt8](response)
                    self.dataFromUDP.update(binz)
                    //self.log("Received from \(ipAddress):\(port):\n\(hexString)\n\(utf8String)\n")
                },
                errorHandler: { [weak self] (error) in
                    guard let self = self else { return }
                    //self.log("Error: \(error)\n")
            })
        } catch {
            //log("Error: \(error)\n")
        }
        
    }
    
    func sendUDP (bytes : [UInt8]) {
        
        let d = Data(bytes: bytes, count: bytes.count )
        
        try! conn?.sendBroadcast(d)
        
    }
    
    private func hexBytes(data: Data) -> String {
        return data
            .map { String($0, radix: 16, uppercase: true) }
            .joined(separator: ", ")
    }



}
