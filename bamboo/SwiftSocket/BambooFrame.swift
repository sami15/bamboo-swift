//
//  BambooFrame.swift
//  bamboo
//
//  Created by toppantaro on 2019/08/28.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation

struct BambooFrame {
    
    let data : [UInt8]
    
    init( segment : BambooEntry ) {
        
        //header
        let ebin = segment.binary
        
        var message = [UInt8]()
        message += "BAMB0000".utf8
        //message += UInt8(0)
        //message += UInt16(ebin.count)
        message += ebin
        data = message
        
    }
    
    
}
