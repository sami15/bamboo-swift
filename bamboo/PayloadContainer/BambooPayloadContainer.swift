//
//  BambooPayloadContainer.swift
//  bamboo
//
//  Created by toppantaro on 2019/08/28.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation

struct TanglePair {
    let author : String
    let sequenceNumber : UInt64
    
    var binary : [UInt8] {
        get {
            var xdata = [UInt8]()
            xdata += author.utf8
            let seq = varu64.init(num: sequenceNumber)
            xdata += seq.bytes
            return xdata
        }
    }
}

struct BambooContainerTangle {
    
    let data: [TanglePair]
    
    var binary : [UInt8] {
        
        var xdata = [UInt8]()
        for i in data {
            xdata += i.binary
        }
        return xdata
    }
}
enum BambooPayloadContainerType {
    case post
    case comment     //links to commented post
    case like      //liked post
    
}

struct BambooPayloadContainer {
    
    
    let type : BambooPayloadContainerType
    let target : TanglePair?    //link to commented or liked post   
    let tangle : BambooContainerTangle?
    let data : String
    
    var binary : [UInt8] {
        get {
            var xdata = [UInt8]()
            //xdata = xdata + author.binary  //
            if (target != nil) {
                xdata = xdata + target!.binary
            }
            if (tangle != nil) {
                xdata = xdata + tangle!.binary
            }
            let stringData = data.utf8
            xdata = xdata + stringData
            
            let leng = UInt64 (stringData.count)
            let seq = varu64.init(num: leng)
            xdata += seq.bytes
            
            return xdata
            
        }
    }
    
}
