//
//  BambooEntry.swift
//  bamboo
//
//  Created by toppantaro on 2019/08/19.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation


struct BambooEntry : Hashable , Equatable {
    
    let author : YamfSignatory //needs to be edkey
    let backLink : YamfHash?
    let isEndOfFeed : Bool
    let lipmaaLink : YamfHash?
    let payloadHash : YamfHash
    let payloadSize : UInt64 //VarU64
    let sequenceNumber : UInt64 //VarU64
    var signature : YamfHash? = nil //if no signature passed, create one
    
    static func == (lhs: BambooEntry, rhs: BambooEntry) -> Bool {
        return lhs.signature == rhs.signature
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(signature)
    }
    
    mutating func sign ( privateKey : String ){
        
        //compile the data into byte array
        var data = [UInt8]()
        data = data + author.binary  //
        if (backLink != nil) {
            data = data + backLink!.binary
        }
        if (lipmaaLink != nil) {
            data = data + lipmaaLink!.binary
        }
        data = data + payloadHash.binary
        let psz = varu64.init(num: payloadSize)
        data = data + psz.bytes
        let seq = varu64.init(num: sequenceNumber)
        data = data + seq.bytes
        let yhash = Blake2b.hash(data: data)
        signature = YamfHash(Hash:yhash.hexDescription() )
        
        //set signature
        
    }
    
    var binary : [UInt8] {
        get {
            var data = [UInt8]()
            data = data + author.binary  //
            if (backLink != nil) {
                data = data + backLink!.binary
            }
            if (lipmaaLink != nil) {
                data = data + lipmaaLink!.binary
            }
            data = data + payloadHash.binary
            let psz = varu64.init(num: payloadSize)
            data = data + psz.bytes
            let seq = varu64.init(num: sequenceNumber)
            data = data + seq.bytes
            let yhash = Blake2b.hash(data: data)
            data = data + yhash
            return data
        }
    }
}





