//
//  payloadStore.swift
//  bamboo
//
//  Created by toppantaro on 2019/08/21.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation

struct BambooPayload {
    
    var data : [UInt8] {
        get {
            return container.binary
        }
    }
    let sizeInBytes : UInt64
    let Hash : String //do this with big number
    let container : BambooPayloadContainer
    
}

class PayloadStore {
    
    var list=[BambooPayload]()
    var entryHashes = [String:Int]() //hash points to item on list
    
    func append (payload : BambooPayload) {
        
        let listIndex = list.count
        entryHashes[payload.Hash] = listIndex
        list.append(payload)
        
    }
    
    var everything : [BambooPayload] {
        get {
            return list
        }
    }
    
    /* func payloadHelperDEPRE (text : String) -> BambooPayload {
        
        let dat = Array(text.data(using: .utf8)!)
        let siz = UInt64(dat.count)
        let hashString = Blake2b.hash(data: dat)
        //return BambooPayload(data: dat, sizeInBytes: siz, Hash: hashString.hexDescription())
    } */
    
    func payloadHelper (containerType: BambooPayloadContainerType , target : TanglePair? , tangle : BambooContainerTangle? , text : String) -> BambooPayload {
        
        let container = BambooPayloadContainer(type: containerType, target: target, tangle: tangle, data: text)
        let dat = container.binary
        let siz = UInt64(dat.count)
        let hashString = Blake2b.hash(data: dat)
        return BambooPayload(sizeInBytes: siz, Hash: hashString.hexDescription(), container: container)
     }
    
}
