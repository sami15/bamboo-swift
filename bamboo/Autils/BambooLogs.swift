//
//  BambooLogWriter.swift
//  bamboo
//
//  Created by toppantaro on 2019/08/20.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation

class BambooLogs {

    var list = [BambooLog]()
    
    func appendNewLog () {
        
        
    }
    
    func getAllEntries( userNumber : Int ) -> [BambooEntry] {
        
        return list[userNumber].list
        
    }
    
    func append (entry : BambooEntry) -> Bool {
        
        var logs = list.filter { $0.author == entry.author }
        var blog : BambooLog
        
        if logs.isEmpty {
            blog = BambooLog(nauthor: entry.author)
            list.append(blog)
        } else {
            blog = logs[0]
        }
        
        blog.append(credentials: entry.author, entry: entry)
        
        return true
        
    }
    
    func findAvailableSeqNumber (credentials : YamfSignatory) -> UInt64 {
        
        var logs = list.filter { $0.author == credentials }
        
        if logs.isEmpty {
            return 1
        } else {
            return logs[0].availableSeqNumber
        }
    }
    
    func entryBackLink (credentials : YamfSignatory, seq : UInt64 ) -> YamfHash? {
        
        var logs = list.filter { $0.author == credentials }
        
        if logs.isEmpty {
            return nil
        }
        return logs[0].entryBackLink( seq : seq )
        
    }
    
    func entryLipmaaLink (credentials : YamfSignatory , seq : UInt64 ) -> YamfHash? {
        
        var logs = list.filter { $0.author == credentials }
        
        if logs.isEmpty {
            return nil
        }
        
        return logs[0].entryLipmaaLink(seq: seq )
        
    }
    
    func calcLipmaa (seq : UInt64) -> UInt64 {
        
        //HACK
        if (seq==1) { return 1}
        var m : UInt64 = 1
        var po3 : UInt64 = 3
        var x : UInt64 = seq
        
        while m < seq {
            
            po3 = po3 * 3
            m = (po3 - 1) / 2       //autofloor with uint?
           
        
        }
        
        po3 = po3 / 3
        
        if m != seq {
                
                while  x != 0 {
                    m = (po3 - 1) / 2
                    po3 = po3 / 3
                    x %= m
                }
                if m != po3 {
                    po3 = m
                        
                }
            }
        
        
        let res = (seq - po3)
        return res
        
    }
    
    
    func createNewEntryHelper (credentials : YamfSignatory, payload : BambooPayload , seq : UInt64? ) -> BambooEntry {
        
        //if seq is null, find available seq
        //let previousEntryHash = "d0d0"
        //let lipmaaLinkHash = "dad0"
        
        let payloadSize = payload.sizeInBytes //payload.lengthOfBytes(using: UTF8) //payload.lengthOfBytes(using: UTF8) as UInt64
        
        //let payloadHash = Blake2b.hash(text: payload)
        //let payloadHashHex = payloadHash.hexDescription()
        let payloadHashYamf = YamfHash(Hash: payload.Hash)
        let seqNumber = findAvailableSeqNumber(credentials: credentials)
        let previousSeqNumber = seqNumber - 1 //HACK
        let lipMaaSeqNumber = calcLipmaa(seq: seqNumber)
        //these guys return nil if no previous entries can be found
        let backlinkHash = entryBackLink(credentials: credentials , seq : previousSeqNumber)
        var lipmaaLinkHash = entryLipmaaLink(credentials: credentials, seq : lipMaaSeqNumber)
        
        /*
        if (backlinkHash != nil) {
            if backlinkHash == lipmaaLinkHash {
                lipmaaLinkHash = nil    //omit lipmaa link if its the same as previous entry
            }
        }
        */
        
        //let backlink = YamfHash(Hash: previousEntryHash)
        //let lipmaaLink = YamfHash(Hash: lipmaaLinkHash)
        
        let logItem = BambooEntry(author: credentials, backLink: backlinkHash, isEndOfFeed: false, lipmaaLink: lipmaaLinkHash, payloadHash: payloadHashYamf, payloadSize: payloadSize, sequenceNumber: seqNumber, signature: nil)
        
        //sign it later
        //logItem.sign(privateKey: <#T##String#>)
        
        return logItem
        
    }
    
    func findSegmentByPubkeyAndSeqnum ( credentials : YamfSignatory, seq : UInt64?) -> BambooEntry? {
        
        let pin = list.filter { $0.author == credentials }
        if pin.isEmpty { return nil }
        
        if (seq != nil) {
            let see = pin[0].list.filter{ $0.sequenceNumber == seq }
            if see.isEmpty { return nil }
            return see[0]
        }
        
        let lastSeq = pin[0].list.last
        let see = pin[0].list.filter{ $0.sequenceNumber == lastSeq?.sequenceNumber }
        if see.isEmpty { return nil }
        return see[0]
        
    }
    
    var everything : [BambooEntry] {
        get {
            
            var eve = [BambooEntry]()
            for i in list {
                for ii in i.list {
                    eve.append(ii)
                }
                
            }
            return eve
        }
    }
    
}
