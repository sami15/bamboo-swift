//
//  BambooLog.swift
//  bamboo
//
//  Created by toppantaro on 2019/08/20.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation

class BambooLog {
    
    var list=[BambooEntry]()
    var entryHashes = [String:Int]() //hash points to item on list
    var verifiedEntriesCache = [String:Bool]() //previously verified entries on this log
    var verifiedAsLog = false //enough data to verify this log?
    var author : YamfSignatory? = nil   //set this with first entry
    var availableSeqNumber : UInt64 = 0
    
    init (nauthor : YamfSignatory) {
        author = nauthor
    }
    
    func entryBackLink ( seq : UInt64 ) -> YamfHash? {
        
        if list.isEmpty { return nil }
        let en = list.filter { $0.sequenceNumber == seq }
        if en.isEmpty { return nil }
        return en[0].payloadHash
    }
    
    func entryLipmaaLink ( seq : UInt64 ) -> YamfHash? {
        
        if list.isEmpty { return nil }
        let en = list.filter { $0.sequenceNumber == seq }
        if en.isEmpty { return nil }
        return en[0].payloadHash
        
    }
    
    
    func isAllowedToAppend (credentials : YamfSignatory ) -> Bool {
       
            if list.isEmpty { return true }
            
            //map authors
            
            //check if credentials match
            
            let first = list.filter{ $0.sequenceNumber == 1 }
            if first.isEmpty {
                //no seq number for first entry, we are missing seq number 0 for some reason
                
                return false
            }
            //
            return true
        
    }
    
    func append ( credentials : YamfSignatory,  entry : BambooEntry ) -> Bool {
        
        //take care of bamboo specifics
        
        if !isAllowedToAppend(credentials: credentials) {
            
            return false
        }
        if entry.sequenceNumber == nil {
            
            
        }
        
        //verify first before inserting. verify your own shit too
        //if lipmaa chain verifies, we are ok
        
        
        //adding stuff might be in random order, default to non verified
        //allow throwing out of sync entries, update state each time
        verifiedAsLog = verifyLipmaaChain(entry: entry)
        //do we care about the order, maybe not
        
        
        let listIndex = list.count
        list.append(entry)
        entryHashes[entry.payloadHash.Hash] = listIndex
        
        if (entry.sequenceNumber >= availableSeqNumber) {
            availableSeqNumber = entry.sequenceNumber + 1
        }
        return true
    }
    
    /* func verifiedEntries() -> [BambooEntry] {
        
        //verify as we add
        
        //if chain is cons
        
    } */
    
    func verify ( entry : BambooEntry , credentials : YamfSignatory ) -> Bool {
        
        //fast verify
        
        //authenticity has been checked via the signature (matching the claimed author)
        //the entry has sequence number one OR there is a link path from the entry to the first entry, where all but the newest entry (which is the one to be verified) are already verified
        //the entry has sequence number one OR the backlink and the lipmaalink point to the entry of the expected sequence number
        /* if let _ = verifiedEntriesCache[entry.backLink.hash] {
            return true
        }
        if let _ = verifiedEntriesCache[entry.lipmaaLink.hash] {
            return true
        }
        
        let previousEntry = entryHashes[entry.backLink.hash]
        let lipmaaEntry = entryHashes[entry.lipmaaLink.hash]
        
        if (previousEntry == nil && lipmaaEntry == nil ) {
            
            return false
        }
        */
        
        
        return false
    }
    
    func verifyLipmaaChain (entry : BambooEntry) -> Bool {
        
        //start from the observed entry, go back as far as we can go
        //the thing we are verifying is not on the log yet
        if (entry.backLink == nil) { return false }
        
        /* if verifiedAsLog {
            
            verifiedEntriesCache[entry.payloadHash.Hash] = true
            return true
            
        } */
        
        let bemtr = [BambooEntry]()
        guard let lipLinks = existingLipmaaChain(entry: entry, chain: bemtr) else {
            return false
        }
        //lipmaa entries are verified for hash when inserted
        //lets pretend the hash matches
        let seqs = lipLinks.map{ $0.sequenceNumber}
        //print (seqs)
        
        //maybe should return verified until xx, from newest to latest
        if seqs[0] != 1 {
            verifiedAsLog = false
            return false    //no root to first, no game
        }
        //step 2
        //verify from end to start for hash corruption
        
        
        verifiedAsLog = true
        //we got shit missing, this is not verified yet
        return true
        
    }
    
    func existingLipmaaChain(entry : BambooEntry, chain : [BambooEntry] ) -> [BambooEntry]? {
        
        //recursive search to establish verifiability
        //var pchain = chain
        guard let enid = entry.lipmaaLink?.Hash else {
            return nil
        }
        //print(enid)
        if let lipmaaEntryID = entryHashes[enid] {
            let nextEnt = list[lipmaaEntryID]
            var chin = [BambooEntry]()
            chin.append(nextEnt)
            chin += chain
            
            //print (nextEnt.payloadHash.Hash)
            if let c = existingLipmaaChain(entry: nextEnt , chain: chin ) {
                
                return c
                
                }
            
            return chin
            
        }
        
        return nil
        
    }
    
    
    
    
}
