//
//  File.swift
//  bamboo
//
//  Created by toppantaro on 2019/07/01.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation

enum ByteNumber : Int {
    case One = 248
    case Two
    case Three
    case Four
    case Five
    case Six
    case Seven
    case Eight
}

typealias VarU64 = [UInt8];

struct varu64 {
    var bytes : [UInt8] = [UInt8]()
    init(num: UInt64){
        self.bytes = self.encode(num: num)
    }
    mutating func encode( num: UInt64 ) -> VarU64 {
        //gives an array of bytes
        var bytearr : [UInt8] = [UInt8]()
        
        if num > ByteNumber.One.rawValue {
            var num_bytes = 0;
            var j = num;
            while j > 0 {
                j = j >> 8
                num_bytes = num_bytes + 1
            }
            //the first byte then contains a header with the lenght
            let headerWithLenght = ByteNumber.One.rawValue + (num_bytes-1)
            var numb = num; //copy immutable passed in value
            bytearr.append( UInt8(headerWithLenght)) ; //ByteNumber::One as u8 - 1 + num_bytes;
            let d = Data(bytes: &numb,
                         count: MemoryLayout.size(ofValue: numb))
            var f=0;
            while (f<num_bytes){
                
                bytearr.append(d[f])//f+1]=d[f]
                f=f+1
            }
            //BigEndian::write_uint(&mut bytes[1..], num, num_bytes as usize);
            //&bytes[0..num_bytes as usize + 1]
            return bytearr;
        }
    
        return bytearr;
    }
    
    func decode () -> UInt64 {
    
        //decode this
        let first_byte = Int(self.bytes[0]);
        let ded = ByteNumber.One.rawValue;
        if first_byte >= ded {
            let num_bytes = (first_byte - ded)+1
            let ra = self.bytes[1...num_bytes]
            
            let data = Data(ra)
            let decimalValue = data.reduce(0) { v, byte in
                return v << 8 | Int(byte)
            }
            return UInt64(decimalValue)
            
            //let num_bytes = first_byte - ByteNumber::One as u8 + 1;
            //BigEndian::read_uint(&bytes[1..], num_bytes as usize)
            
        } else {
            
            return UInt64(first_byte)
        }
        
    
    }
}
