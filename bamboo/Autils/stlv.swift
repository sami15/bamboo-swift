//
//  stlv.swift
//  bamboo
//
//  Created by toppantaro on 2019/08/19.
//  Copyright © 2019 toppantaro. All rights reserved.
//

import Foundation

struct STLV {
    
    let data : [UInt8]
    
    init (type : UInt64 , value: UInt64 ) {
        
        let typeV64 = varu64.init(num: type)
        let valueV64 = varu64.init(num: value)
        let l = (typeV64.bytes.count + valueV64.bytes.count)
        let ll = UInt64(l)
        let len = varu64.init(num: ll )
        data = typeV64.bytes + len.bytes + valueV64.bytes
        
    }
    
    
}

/*
Stlv is a binary encoding for type-length-value. The type is an unsigned 64 bit integer, the value can have a length of up to 2^64 - 1 bytes.

The binary encoding is defined as the concatenation of:

the type, encoded as a VarU64
the length of the value, encoded as a VarU64
the value itself
 */

